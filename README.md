# README #

### What is this repository for? ###

* This is a practice project using AngularJS to create a movie management app.
* Version 1.0.0.0

### How do I get set up? ###

* Prerequisites:
    * node/npm installed. 
    * Also install `http-server` using `npm install -g http-server`
* To run:
    * open command prompt and `cd` into folder with code
    * type `http-server`
    * open your browser and go to `http://localhost:8080`

### Who do I talk to? ###

* [Email Me](mailTo:em1419@cs.ship.edu)