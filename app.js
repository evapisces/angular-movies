var app = angular.module('movies-app', ['ngRoute', 'ngStorage', 'toastr']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: "partials/home.html",
            controller: "MoviesCtrl"
        })
        .when('/add', {
            templateUrl: "partials/add.html",
            controller: "MoviesCtrl"
        })
        .when('/edit/:id', {
            templateUrl: "partials/edit.html",
            controller: "MoviesCtrl"
        })
        .otherwise({redirectTo: '/home'});
});

// Service to get the movies data from the json file
app.factory('MoviesService', function($http, $q) {
    var defer = false;

    return {
        getMovies: function() {
            if (!defer) {
                defer = $q.defer();
                $http.get('movies.json').then(function(data) {
                    console.log('movies.json loaded');
                    defer.resolve(data);
                });
            }

            return defer.promise;
        }
    }
});

// Controller
app.controller("MoviesCtrl", ['$scope', '$routeParams', '$location', '$localStorage', 'toastr', 'MoviesService', function($scope, $routeParams, $location, $localStorage, toastr, MoviesService) {
    $scope.movies = [];
    $scope.movie = {};


    MoviesService.getMovies()
        .then(function(result) {
            $localStorage.movies = result.data.movies;
    });

    $scope.movies = $localStorage.movies;
    $localStorage.movieCount = $scope.movies.length;

    // If the user is on the EDIT page, then grab the data for the movie chosen
    // from local storage
    if ($location.path().indexOf('edit') > -1) {
        var id = $routeParams.id;
        $scope.movie = $localStorage.movies[id-1];

        $location.path('/edit/' + id);
    }

    // Go to ADD page
    $scope.add = function() {
        $location.path('/add');
    }

    // Add a new movie to local storage
    $scope.addMovie = function(movie) {
        var movie = {
            id: $localStorage.movieCount + 1,
            title: movie.title,
            year: movie.year,
            stars: movie.stars
        };

        $localStorage.movies.push(movie);
        $localStorage.movieCount++;

        toastr.info(movie.title + " added successfully!");

        // redirect to main movies page
        $location.path('/home');
    }

    // Go to EDIT page
    $scope.edit = function (id) {
        $location.path('/edit/' + id);
    }

    // Update a given movie
    $scope.updateMovie = function(movie) {
        $localStorage.movies[movie.id-1].title = movie.title;
        $localStorage.movies[movie.id-1].year = movie.year;
        $localStorage.movies[movie.id-1].stars = movie.stars;

        toastr.info(movie.title + "  was successfully updated!");

        // redirect to main movies page
        $location.path('/home');
    }
}]);
